## rsynth

The late Nick Ing-Simmons' [rsynth](https://sourceforge.net/projects/rsynth/) was a C++ speech synthesizer using Klatt-style formant synthesis. The last release was in 2005. In 2013, Jari Komppa updated and refactored rsynth for the [SoLoud](http://solhsa.com/soloud/) audio engine and removed features to make the project indisputably public domain.

This repository is yet another fork of rsynth, based on Komppa's work.
